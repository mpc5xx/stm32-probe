#ifndef __BDM_SIGNALS_H__
#define __BDM_SIGNALS_H__

#include <stdint.h>

#include "stm32l4xx_hal.h"
#include "delay.h"

inline static void dsck(uint8_t v){
        HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, v ? GPIO_PIN_SET : GPIO_PIN_RESET);
}

inline static void dsdi(uint8_t v){
        HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, v ? GPIO_PIN_SET : GPIO_PIN_RESET);
}

inline static uint8_t dsdo(){
	uint8_t v;
	v = HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_11) ? 1 : 0;
	return v;
}

inline static uint8_t vfls0(){
	uint8_t v;
	v = HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_8) ? 1 : 0;
	return v;
}

inline static uint8_t bdm_cycle(uint8_t i){
	uint8_t o;
	o = dsdo();
	dsdi(i);
	/* DSDI input data setup time at least 12ns (MPC555) */
	dsck(1);
	/* clock pulse width must be at least 50ns (for MPC555@33MHZ) */
	dsck(0);
	return o;
}

typedef struct {
	uint8_t mode;
	uint8_t control;
	uint32_t data;
} bdm_in_t;

typedef struct {
	uint8_t status[2];
	uint32_t data;
} bdm_out_t;

/* return 1 if not ready */
inline static int bdm_com(bdm_in_t in, bdm_out_t *out){
	uint8_t ready;
	int nbits;
	int i;

	if(out == NULL){
		return 1;
	}

	bdm_cycle(0);

	int ntry;
	ntry = 20;
	while(ntry>0 && bdm_cycle(0) != 0){
		ntry--;
		delay(1);
	}
	if(ntry==0){
		return 1;
	}

	/* start */
	if(bdm_cycle(1) != 0){
		return 1;
	}

	/* mode and control bits */
	out->status[0] = bdm_cycle(in.mode);
	out->status[1] = bdm_cycle(in.control);

	/* data */
	out->data = 0;
	nbits = in.mode ? 7 : 32;
	for(i=nbits-1;i>=0;i--){
		/* MSB first */
		out->data |= bdm_cycle(in.data & (1 << i) ? 1 : 0) << i;
	}

	return 0;
}

#endif /* __BDM_SIGNALS_H__ */
