#include <stdint.h>

#include "io.h"
#include "delay.h"
#include "bdm.h"

int bdm_nop(bdm_out_t *out){
	bdm_in_t in;

	in.mode=1;
	in.control=1;
	in.data=0;
	if(bdm_com(in, out)){
		return 1;
	}
	return 0;
}

static void bdm_out_print(bdm_out_t out){
	io_printf("out: %d %d 0x%x\r\n", out.status[0], out.status[1],
		out.data);
}

int bdm_gpr_read(uint8_t reg, uint32_t *d){
	bdm_in_t in;
	bdm_out_t out;

	if(d == NULL){
		return 1;
	}

	if(reg > 31){
		return 1;
	}

	in.mode=0;
	in.control=0;
	in.data=0x7c169ba6 | (reg << 21); /* mtspr 630, reg */
	if(bdm_com(in, &out)){
		return 1;
	}
	if(out.status[0] != 1 || out.status[1] != 1 || out.data != 0xffffffff){
		return 1;
	}
	in.mode=0;
	in.control=0;
	in.data=0x60000000; /* nop */
	if(bdm_com(in, &out)){
		return 1;
	}
	if(out.status[0] != 0 || out.status[1] != 0){
		return 1;
	}
	d[0] = out.data;
	return 0;
}

int bdm_gpr_write(uint8_t reg, uint32_t v){
	bdm_in_t in;
	bdm_out_t out;

	if(reg > 31){
		return 1;
	}

	in.mode=0;
	in.control=0;
	in.data=0x7c169aa6 | (reg << 21); /* mfspr reg, 630 */
	if(bdm_com(in, &out)){
		return 1;
	}
	if(out.status[0] != 1 || out.status[1] != 1 || out.data != 0xffffffff){
		return 1;
	}
	in.mode=0;
	in.control=1;
	in.data=v;
	if(bdm_com(in, &out)){
		return 1;
	}
	if(out.status[0] != 1 || out.status[1] != 1 || out.data != 0xffffffff){
		return 1;
	}
	return 0;
}

int bdm_spr_read(uint32_t spr, uint32_t *d){
	bdm_in_t in;
	bdm_out_t out;
	uint32_t r1;

	if(d == NULL){
		return 1;
	}

	if(spr > 0x3ff){
		return 1;
	}

	/* DPDR is not allowed */
	if(spr == 630){
		return 1;
	}


	if(bdm_gpr_read(1, &r1)){
		return 1;
	}

	in.mode=0;
	in.control=0;
	in.data=0x7c2002a6 | ((spr & 0x1f) << 16) | (((spr >> 5) & 0x1f) << 11); /* mfspr r1,spr */
	if(bdm_com(in, &out)){
		return 1;
	}
	if(out.status[0] != 1 || out.status[1] != 1 || out.data != 0xffffffff){
		return 1;
	}
	if(bdm_gpr_read(1,d)){
		return 1;
	}
	if(bdm_gpr_write(1,r1)){
		return 1;
	}
	return 0;
}

int bdm_spr_write(uint32_t spr, uint32_t v){
	bdm_in_t in;
	bdm_out_t out;
	uint32_t r1;

	if(spr > 0x3ff){
		return 1;
	}

	/* DPDR is not allowed */
	if(spr == 630){
		return 1;
	}

	if(bdm_gpr_read(1, &r1)){
		return 1;
	}
	if(bdm_gpr_write(1,v)){
		return 1;
	}
	in.mode=0;
	in.control=0;
	in.data=0x7c2003a6 | ((spr & 0x1f) << 16) | (((spr >> 5) & 0x1f) << 11); /* mtspr spr,r1 */
	if(bdm_com(in, &out)){
		return 1;
	}
	if(out.status[0] != 1 || out.status[1] != 1 || out.data != 0xffffffff){
		return 1;
	}
	if(bdm_gpr_write(1,r1)){
		return 1;
	}
	return 0;
}

/* size = 1, 2 or 4 */
int bdm_mem_read(int size, uint32_t addr, uint32_t *d){
	bdm_in_t in;
	bdm_out_t out;
	uint32_t r1;
	uint32_t r2;

	if(size != 1 && size != 2 && size != 4){
		return 1;
	}
	if(d == NULL){
		return 1;
	}

	if(bdm_gpr_read(1, &r1) || bdm_gpr_read(2, &r2)){
		return 1;
	}
	if(bdm_gpr_write(1, addr)){
		return 1;
	}

	/* read memory:
	size 1: lbz r2, 0(r1) 0x88410000
	size 2: lhz r2, 0(r1) 0xa0410000
	size 4: lwz r2, 0(r1) 0x80410000
	*/
	in.mode=0;
	in.control=0;
	in.data = size==1 ? 0x88410000 : ( size==2 ? 0xa0410000 : 0x80410000);
	if(bdm_com(in,&out)){
		return 1;
	}
	if(out.status[0] != 1 || out.status[0] != 1 || out.data != 0xffffffff){
		return 1;
	}

	if(bdm_gpr_read(2, d)){
		return 1;
	}
	if(bdm_gpr_write(1, r1) || bdm_gpr_write(2, r2)){
		return 1;
	}
	return 0;
}

/* size = 1, 2 or 4 */
int bdm_mem_write(int size, uint32_t addr, uint32_t v){
	bdm_in_t in;
	bdm_out_t out;
	uint32_t r1;
	uint32_t r2;

	if(size != 1 && size != 2 && size != 4){
		return 1;
	}

	if(bdm_gpr_read(1, &r1) || bdm_gpr_read(2, &r2)){
		return 1;
	}
	if(bdm_gpr_write(1, addr) || bdm_gpr_write(2, v)){
		return 1;
	}

	/* read memory:
	size 1: stb r2, 0(r1) 0x98410000
	size 2: sth r2, 0(r1) 0xb0410000
	size 4: stw r2, 0(r1) 0x90410000
	*/
	in.mode=0;
	in.control=0;
	in.data = size==1 ? 0x98410000 : ( size==2 ? 0xb0410000 : 0x90410000);
	if(bdm_com(in,&out)){
		return 1;
	}
	if(out.status[0] != 1 || out.status[0] != 1 || out.data != 0xffffffff){
		return 1;
	}

	if(bdm_gpr_write(1, r1) || bdm_gpr_write(2, r2)){
		return 1;
	}
	return 0;
}

int bdm_internal_base_addr_read(uint32_t *addr){
	uint32_t v;
	if(addr==NULL){
		return 1;
	}
	if(bdm_spr_read(638, &v)){
		return 1;
	}
	v = (v >> 1) & 0x7;
	addr[0] = v << 21;
	return 0;
}

int bdm_init(){
	int ntry;
	bdm_out_t out;
	int i;

	dsck(1);
	dsdi(0);

	io_printf("De-assert #RESET now\r\n");

	ntry=20;
	while(ntry > 0 && vfls0() == 0){
		ntry--;
		delay(500);
	}

	if(vfls0() == 0){
		return 1;
	}

	dsck(0);

	if(bdm_nop(&out)){
		return 1;
	}
	/* checking NULL state */
	if(out.status[0] != 1 || out.status[1] != 1|| out.data != 0x7f){
		return 1;
	}

	/* ib: internal base address */
	uint32_t ib;
	if(bdm_internal_base_addr_read(&ib)){
		return 1;
	}
	io_printf("internal base addr = 0x%x\r\n", ib);

	uint32_t v;

	/* disable watchdog */
	if(bdm_mem_read(4, ib+0x002fc004, &v)){
		return 1;
	}
	io_printf("SYPCR [0x%x] = 0x%x\r\n", 0x002fc004, v);
	v &= ~(1 << 2);
	if(bdm_mem_write(4, ib+0x002fc004, v)){
		io_printf("write sypcr failed\r\n");
		return 1;
	}
	io_printf("disabled watchdog...\r\n");
	if(bdm_mem_read(4, ib+0x002fc004, &v)){
		return 1;
	}
	io_printf("SYPCR [0x%x] = 0x%x\r\n", 0x002fc004, v);

	return 0;
}
