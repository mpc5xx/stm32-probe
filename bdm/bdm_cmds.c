#include <stdio.h>

#include "io.h"
#include "cmd.h"
#include "bdm.h"

int cmd_bdminit(char *args){
	if(bdm_init()){
		io_printf("Debug mode failed\r\n");
		return 1;
	}

	io_printf("Ok\r\n");
	return 0;
}

/* bdmcom mode control data */
int cmd_bdmcom(char *args){
	bdm_in_t in;
	bdm_out_t out;

	if(sscanf(args, "%*s %d %d %x", &in.mode, &in.control, &in.data) != 3){
		io_printf("Bad arguments\r\n");
		return 1;
	}
	if(in.mode != 0 && in.mode != 1){
		io_printf("Bad arguments\r\n");
		return 1;
	}

	if(in.control != 0 && in.control != 1){
		io_printf("Bad arguments\r\n");
		return 1;
	}

	if(bdm_com(in, &out)){
		io_printf("bdm_com failed\r\n");
		return 1;
	}

	io_printf("status: %d %d\r\n", out.status[0], out.status[1]);
	io_printf("data: %x\r\n", out.data);
	
	return 0;
}

int cmd_gpr(char *args){
	uint32_t reg;
	uint32_t v;
	uint32_t prev;
	int nargs;

	nargs = sscanf(args, "%*s %d %x", &reg, &v);
	if(nargs != 1 && nargs != 2){
		io_printf("Bad arguments\r\n");
		return 1;
	}
	if(reg > 31){
		io_printf("Bad arguments\r\n");
		return 1;
	}
	if(nargs == 1){
		/* read */
		if(bdm_gpr_read(reg, &v)){
			io_printf("bdm_gpr_read failed\r\n");
			return 1;
		}
		io_printf("r%d = 0x%x\r\n", reg, v);
	}else{ /* nargs == 2 */
		/* write */
		if(bdm_gpr_read(reg, &prev)){
			io_printf("bdm_gpr_read failed\r\n");
			return 1;
		}
		if(bdm_gpr_write(reg, v)){
			io_printf("bdm_gpr_write failed\r\n");
			return 1;
		}
		io_printf("r%d (0x%x) <- 0x%x\r\n", reg, prev, v);
	}
	return 0;
}

int cmd_spr(char *args){
	uint32_t spr;
	uint32_t v;
	uint32_t prev;
	int nargs;

	nargs = sscanf(args, "%*s %d %x", &spr, &v);
	if(nargs != 1 && nargs != 2){
		io_printf("Bad arguments\r\n");
		return 1;
	}
	if(spr > 0x3ff){
		io_printf("Bad arguments\r\n");
		return 1;
	}
	if(nargs == 1){
		/* read */
		if(bdm_spr_read(spr, &v)){
			io_printf("bdm_spr_read failed\r\n");
			return 1;
		}
		io_printf("spr %d = 0x%x\r\n", spr, v);
	}else{ /* nargs == 2 */
		/* write */
		if(bdm_spr_read(spr, &prev)){
			io_printf("bdm_spr_read failed\r\n");
			return 1;
		}
		if(bdm_spr_write(spr, v)){
			io_printf("bdm_spr_write failed\r\n");
			return 1;
		}
		io_printf("spr %d (0x%x) <- 0x%x\r\n", spr, prev, v);
	}
	return 0;
}

int cmd_mem(char *args){
	int size;
	uint32_t addr;
	uint32_t v;
	uint32_t prev;
	int nargs;

	nargs = sscanf(args, "%*s %d %x %x", &size, &addr, &v);
	if(nargs != 2 && nargs != 3){
		io_printf("Bad arguments\r\n");
		return 1;
	}
	if(size != 1 && size != 2 && size != 4){
		io_printf("Bad arguments\r\n");
		return 1;
	}
	if(nargs == 2){
		/* read */
		if(bdm_mem_read(size, addr, &v)){
			io_printf("bdm_addr_read failed\r\n");
			return 1;
		}
		io_printf("[%x] = 0x%x\r\n", addr, v);
	}else{ /* nargs == 3 */
		/* write */
		if(bdm_mem_read(size, addr, &prev)){
			io_printf("bdm_addr_read failed\r\n");
			return 1;
		}
		if(bdm_mem_write(size, addr, v)){
			io_printf("bdm_addr_write failed\r\n");
			return 1;
		}
		io_printf("[%x] (0x%x) <- 0x%x\r\n", addr, prev, v);
	}
	return 0;
}
