#ifndef __BDM_H__
#define __BDM_H__

#include "bdm_signals.h"


int bdm_gpr_read(uint8_t reg, uint32_t *d);
int bdm_gpr_write(uint8_t reg, uint32_t v);
int bdm_spr_read(uint32_t spr, uint32_t *d);
int bdm_spr_write(uint32_t spr, uint32_t v);
int bdm_mem_read(int size, uint32_t addr, uint32_t *d);
int bdm_mem_write(int size, uint32_t addr, uint32_t v);

int bdm_init();

int cmd_bdminit(char *args);
int cmd_bdmcom(char *args);
int cmd_gpr(char *args);
int cmd_spr(char *args);
int cmd_mem(char *args);

#endif /* __BDM_H__ */
