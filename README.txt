Pinout in ECU (BOSCH)

########################### (edge)

 1   2   3   4   5   6   7
 O   O   O   O   O   O   O

 O   O   O   O   O   O   O
 8   9   10  11  12  13  14

1 - X
2 - VCC (5V)
3 - 12V (ignition?)
4 - DSDI
5 - DSCK
6 - X
7 - RESET (maybe #PORESET)
8 - X
9 - VFLS[1] ?
10 - VFLS[0] ?
11 - DSDO
12 - GND
13 - X
14 - 12V (power?)

Schematics

ECU - nucleo-l432kc - stm32l432kc
DSCK - D12 - PB4 output open-drain
DSDI - D11 - PB5 output open-drain
DSDO - D10 - PA11 input
VFLS[0] - D9 - PA8 input (optional)
