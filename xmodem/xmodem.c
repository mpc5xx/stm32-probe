
#include "io.h"

#define X_SOH 0x01
#define X_STX 0x02
#define X_EOT 0x04
#define X_ACK 0x06
#define X_NAK 0x15
#define X_CAN 0x18

char blkn;

int xmodem_init(){
	char c;
	/* wait <nak> */
	c = io_getchar();
	if(c != X_NAK){
		return 1;
	}
	blkn = 0;
	return 0;
}

int xmodem_send(char *b, int s){
	char c;
	char cksum;
	int i;
	blkn++;
	cksum = 0;
	if(s == 128){
		io_putchar(X_SOH);
	}else if(s == 1024){
		io_putchar(X_STX);
	}else{
		return 1;
	}
	io_putchar(blkn);
	io_putchar(255-blkn);
	for(i=0;i<s;i++){
		cksum =(cksum + b[i]) & 0xff;
		io_putchar(b[i]);
	}
	io_putchar(cksum);
	/* wait ACK */
	c = io_getchar();
	if(c != X_ACK){
		return 1;
	}
	return 0;
}

int xmodem_end(){
	char c;
	/* send EOF */
	io_putchar(X_EOT);
	c = io_getchar();
	if(c == X_ACK){
		return 0;
	}
	return 1;
}
