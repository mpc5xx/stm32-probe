#ifndef __XMODEM_H__
#define __XMODEM_H__

int xmodem_init();
int xmodem_send(char *b, int s);
int xmodem_end();

#endif /* __XMODEM_H__ */
