
#include <stdio.h>
#include <string.h>

#include "io.h"
#include "delay.h"
#include "cmd.h"
#include "bdm.h"

int cmd_void(char *args);
int cmd_test(char *args);

struct cmd_t cmds[] = {
	{"", cmd_void},
	{"test", cmd_test},
	{"bdminit", cmd_bdminit},
	{"bdmcom", cmd_bdmcom},
	{"gpr", cmd_gpr},
	{"spr", cmd_spr},
	{"mem", cmd_mem},
	{0,}
};

void system_init();

int main(void){

	system_init();

	io_printf("XX\r\n");

	bdm_init();

	cmd_loop();

	while(1)
	;

	return 0;
}

int cmd_void(char *args){
	return 0;
}

int cmd_test(char *args){
	io_printf("This is a test!\r\n");
	return 0;
}
